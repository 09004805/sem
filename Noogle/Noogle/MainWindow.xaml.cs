﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;

namespace Noogle
{
    /// <summary>
    /// Loads words from csv file
    /// On button click, checks text content against loaded word list
    /// </summary>
    public partial class MainWindow : Window
    {
        // ====== GLOBALS USED: ====== //

        // Create list to be filled with csv filter words
        public List<string> csvWords =  new List<string>();
        // Create list to be filled with uni names
        public List<string> universities = new List<string>();
        // Create list to be filled with subject names
        public List<string> subjects = new List<string>();


        // Vars to do with valid messages/file operations:
        // Set path of valid file 
        string validPath = @"C:\Users\Matt\Documents\Noogler\valid.txt";
        // Used when switching between valid messages on screen
        int currentMessage;
        // Stores total messages found in valid file
        int lineCount;
        // Stores all messages read from valid file
        string[] validMessages;


        // For checking against undergrad messages
        string[] undergrad = { "Undergraduate", "undergraduate", "U/G", "UG" };
        // For checking against postgrad messages
        string[] postgrad = { "Postgraduate", "postgraduate", "P/G", "PG" };

        


        // ========= GLOBALS END  ============= //

     

        // Loads messages from valid file into array, counts lines
        void FetchMessages()
        {
            // Fill array with lines of all messages from valid file
            validMessages = File.ReadAllLines(validPath);
            // Grab line count to check number of messages
            lineCount = validMessages.Length;
        }


        // Instantiate message handler 
        MessageHandler MessageHandler = new MessageHandler();

        public MainWindow()
        {
            InitializeComponent();
            File_Read();
        }


        // On startup, load both filter word & uni listings from csv files
        void File_Read()
        {
            // LOAD WORD FILTER LIST:
            // Set file path of csv file
            string wordPath = @"C:\Users\Matt\Documents\Noogler\textwords.csv";
            // Setup stream reader for given file path
            using (StreamReader reader = new StreamReader(wordPath))
            {
                // Whilst still reading file
                while (!reader.EndOfStream)
                {
                    // Split each string at commas, to keep first column only
                    string line = reader.ReadLine().Split(',')[0];
                    // Make each line read this way lowercase
                    line.ToLower();
                    // Add each line read this way to list of strings
                    csvWords.Add(line);
                }
            }
            // LOAD UNI LIST:
            string uniPath = @"C:\Users\Matt\Documents\Noogler\University List.csv";
            using (StreamReader reader = new StreamReader(uniPath))
            {
                // Whilst still reading file
                while (!reader.EndOfStream)
                {
                    // Read in a line
                    string line = reader.ReadLine();
                    // Make each line read this way lowercase
                    //line.ToLower();
                    // Add line to list of strings
                    universities.Add(line);
                }
            }
            // LOAD SUBJECTS LIST:
            string subjectPath = @"C:\Users\Matt\Documents\Noogler\subjects.csv";
            using (StreamReader reader = new StreamReader(subjectPath))
            {
                // While still reading file
                while (!reader.EndOfStream)
                {
                    // Read in a line
                    string line = reader.ReadLine();
                    // Make each line read this way lowercase
                    line.ToLower();
                    // Add line to list of strings
                    subjects.Add(line);
                }
            }
        }

        // First iteration; check the message content against hardcoded values, show result. 
        private void submitBtn_Click(object sender, RoutedEventArgs e)
        {
            // First check to make sure the text box isn't blank
            if(string.IsNullOrWhiteSpace(messageTxtBx.Text))
            {
                // Show error message if txtbx is emtpy
                MessageBox.Show("Please enter a message.");
            }
            // otherwise, continue with checking 
            else
            {                
                // Set csv filter list into array
                var badwords = csvWords.ToArray();
                // Setup given message to be checked
                string input = messageTxtBx.Text;
                // If any words in filter list are found in given message
                if (badwords.Any(input.Contains))
                {
                    // Show confirmation message
                    MessageBox.Show("Unacceptable language used. Sending message to Quarantine file.");

                    // Set path of quarantine file
                    string quarantinePath = @"C:\Users\Matt\Documents\Noogler\quarantine.txt";
                    // Declare streamwriter to use to open file, write message
                    using (StreamWriter writer = new StreamWriter(quarantinePath))
                    {
                        // Write message box contents to file on one line
                        writer.WriteLine(messageTxtBx.Text);
                        
                    }
                }
                else
                {
                    // Show confirmation message
                    MessageBox.Show("Acceptable language used. Sending message to Valid file.");

                    // Declare streamwriter to use to open file, write message
                    using (StreamWriter writer = new StreamWriter(validPath, true))
                    {
                        // Write message box contents to file
                        writer.WriteLine(messageTxtBx.Text);
                 
                    }

                }
             
            }
        }

     

        // On Check Valid click, load a message from valid file, check content
        private void ValidBtn_Click(object sender, RoutedEventArgs e)
        {
            FetchMessages();
            // Confirmation message
            MessageBox.Show(lineCount + " messages found.");
            // Fill text box with first message, reset current message to 0
            currentMessage = 0;
            ValidMessageBox.Text = validMessages[currentMessage];          
        }


        // Show next message in messages array
        private void RightValidBtn_Click(object sender, RoutedEventArgs e)
        {
            FetchMessages();
            // So long as current index isn't larger than overall message count
            if(currentMessage < lineCount - 1)
            {
                // Increment current message
                currentMessage += 1;
            }
            // Show that message
            ValidMessageBox.Text = validMessages[currentMessage];
        }

        private void LeftValidBtn_Click(object sender, RoutedEventArgs e)
        {
            FetchMessages();
            // So long as current message index isn't 0
            if(currentMessage > 0)
            {
                // Decrement current message index
                currentMessage -= 1;
            }         
            // Show that message
            ValidMessageBox.Text = validMessages[currentMessage];
        }


        // This is where messages are stored in memory, plus all attributes
        // Scans and sets current message for grad type, subject, unis etc
        private void CheckMessageBtn_Click(object sender, RoutedEventArgs e)
        {
            // Clear message details fields on screen, in case of previous use
            UGPGtext.Text = " ";
            SubjectsLstBx.Items.Clear();
            UnisListBx.Items.Clear();

            // Create a new Message, defined in MessageHandler.cs
            MessageHandler.Message newMessage = new MessageHandler.Message();
       

            // Check for matches between undergrad words in message    
            if (undergrad.Any(ValidMessageBox.Text.ToLower().Contains))
            {
                // Set grad type to undergrad for new message
                newMessage.gradType = "UG";
                // Show result on screen
                UGPGtext.Text = "UG";

            }
            // If no matches above, check against post grad words
            else if (postgrad.Any(ValidMessageBox.Text.ToLower().Contains))
            {
                // Set grad type to postgrad for new message
                newMessage.gradType = "PG";
                // Show result on screen
                UGPGtext.Text = "PG";
            }
            // If neither of the above
            else
            {
                // Neither grad types detected, set to N/A
                newMessage.gradType = "N/A";
                // Show result on screen
                UGPGtext.Text = "N/A";
            }

            // Check for universities mentioned against uni names list
            if (universities.Any(ValidMessageBox.Text.Contains))
            {
                // If a match is found, return matching strings
                var result = universities.Where(x => ValidMessageBox.Text.Contains(x)).ToArray();
                // Set resulting array to new message universities values
                newMessage.universities = result;
                // Update listbox to show matches found
                foreach(string match in result)
                {
                    UnisListBx.Items.Add(match);
                }

                // Cast from array to string
                //string match = string.Join(",", result.ToArray());
            }
            else
            {
                // If not uni names are found, set field to N/A
                newMessage.universities = new string[] { "N/A" };
            }

            // Check for subjects mentioned against subject names list
            if (subjects.Any(ValidMessageBox.Text.ToLower().Contains))
            {
                // If a match is found, return matching strings
                var result = subjects.Where(x => ValidMessageBox.Text.ToLower().Contains(x)).ToArray();
                // Set resulting array to new message subject values
                newMessage.subjects = result;
                // Update listbox to show matches found
                foreach (string match in result)
                {
                    SubjectsLstBx.Items.Add(match);
                }

            }

            // Set message body value for new message
            newMessage.messageBody = ValidMessageBox.Text;

            // Add the created Message to the list of messages defined in MessageHandler
            MessageHandler.Messages.Add(newMessage);           

        }

        // This is a debug/test button used to confirm message structs have been saved in list correctly
        private void TestBtn_Click(object sender, RoutedEventArgs e)
        {
            // Show number of stored messages from this session
            int messageCount = MessageHandler.Messages.Count();
            MessageBox.Show(messageCount.ToString() + " messages saved this session.");
        }
    }
}
