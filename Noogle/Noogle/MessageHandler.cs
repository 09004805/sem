﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Noogle
{
    /// <summary>
    /// This class defines a mesasge struct, and holds a list of all created through the app.
    /// </summary>
   public class MessageHandler
    {
        // Create a list of messages
        public List<Message> Messages = new List<Message>();

        // This is to hold information related to each message checked
        public struct Message
        {
          
            // The body of the message
            public string messageBody;
            // The grad type 
            public string gradType;
            // The subjects mentioned
            public string[] subjects;
            // The universities mentioned
            public string[] universities;
        }
    }
}
